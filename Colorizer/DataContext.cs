﻿using System.ComponentModel;
using System.Text;
using System.Windows.Media;

namespace Colorizer
{
    public class DataContext : INotifyPropertyChanged
    {
        static readonly SolidColorBrush TransparentBrush = new SolidColorBrush(Colors.Transparent);

        string _input;
        public string Input
        {
            get => _input;

            set
            {
                if (_input == value) return;

                _input = value;
                NotifyPropertyChanged(nameof(Input));
                NotifyPropertyChanged(nameof(BgColorBrush));
            }
        }

        public Brush BgColorBrush => string.IsNullOrWhiteSpace(Input) ? TransparentBrush : GetBrushFromInput();

        Brush GetBrushFromInput()
        {
            byte[] hash;
            using (var sha = new System.Security.Cryptography.SHA1Cng())
                hash = sha.ComputeHash(Encoding.ASCII.GetBytes(Input));

            unsafe
            {
                for (var i = 1; i < 6; i++)
                {
                    hash[0] += hash[i * 3];
                    hash[1] += hash[i * 3 + 1];
                    hash[2] += hash[i * 3 + 2];
                }
            }
            return new SolidColorBrush(Color.FromRgb(hash[0], hash[1], hash[2]));
        }

        public event PropertyChangedEventHandler PropertyChanged;

        void NotifyPropertyChanged(string propertyName) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
